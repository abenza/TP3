// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')
var path    = require("path");
var sqlite3 = require('sqlite3').verbose();
var SHA256 = require('crypto-js/sha256');
var app = express();
var db = new sqlite3.Database('db.sqlite');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(cookieParser());

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});


app.get("/", function(req,res,next) {

	db.all("SELECT * FROM sessions where token = ? ", [req.cookies.user], function(err,data) {
		console.log(data);
		if(data.length == 0){
			res.sendFile(path.join(__dirname+'/public/index.html'));
		}else{
			res.sendFile(path.join(__dirname+'/public/acceuil.html'));
		}
	});
});

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/sessions", function(req, res, next) {
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});

app.get("/logout", function(req,res,next) {
	db.run("DELETE FROM sessions WHERE token = ?", [req.cookies.user], function(err,res) {
	});
	res.redirect('/');
})

app.post("/login", function(req, res, next) {
	db.all("SELECT * FROM users where ident = ? and password = ? ;", [req.body.id, req.body.password],  function(err,data){
		if(data.length != 0){
			var token = makeid(req.body.id, req.connection.remoteAddress);
			res.cookie('user', token);
			db.all("SELECT * FROM sessions where ident = ? ", [req.body.id], function(err,data){
				console.log("OK");
				if(data.length == 0){
					db.run("INSERT INTO sessions values (?, ? ) ", [req.body.id, token], function(err,data){
						res.redirect('/');
					});
				}else{
					db.run("UPDATE sessions set token = ? where ident = ? ", [token, req.body.id], function(err,data){
						res.redirect('/');
					});
				}
			});
		}else{
				res.redirect('/');
		}
	})
});


// Serve static files
app.use(express.static('public'));

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});

function makeid(login,ip) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 30; i++){
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	text += ip;
	text += login;

	var hash = SHA256(text).toString();

	return hash;

}
